﻿using System;

namespace CSVCombiner
{
    internal class HelpCommand : ICommand
    {
        string message;

        public HelpCommand(string message = null)
        {
            this.message = message;
        }

        public void Execute()
        {
            Console.WriteLine("");
            if (!string.IsNullOrEmpty(message)) Console.WriteLine(message);
            Console.WriteLine("CSVCombiner usage:");
            Console.WriteLine("Usage");
            Console.WriteLine("csvcombinger [--input=<path>] [--output=<path>] [--key=<index>] [--delimiter=<'char'>] [--header=<Y|N>]");
            Console.WriteLine("Merges all .csv files in a directory into one, ordering rows by a primary key. Duplicate keys are not allowed. All arguments are optional.");
            Console.WriteLine("--input: path containing the .csv files to combine (default: directory where this executable resides)");
            Console.WriteLine("--output: path where the resulting .csv file should be output (default: an output subfolder where this executable resides)");
            Console.WriteLine("--key: 1-based column index for the .csv files' primary key (default: 1)");
            Console.WriteLine("--delimiter: the character used to separate columns in a row (default: ,). Only single character delimiters are supported. Use single or double quotes to specify the character if it would interfere with your console (e.g. semicolon).");
            Console.WriteLine("--header: whether there is a common header that should be taken into account (default: Y). If the headers differ between files, the merging will be unsuccessful.");
            Console.WriteLine("");
        }
    }
}