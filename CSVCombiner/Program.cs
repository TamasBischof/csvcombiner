﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace CSVCombiner
{
    public class Program
    {
        static Dictionary<int, string> lines = new Dictionary<int, string>();

        static void Main(string[] args)
        {
            CommandFactory factory = new CommandFactory();

            try { factory.GetCommand(args).Execute(); }

            catch(Exception e) { Console.Error.WriteLine(e.Message); }
        }
    }
}
