﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CSVCombiner
{
    /// <summary>
    /// Takes command line arguments and returns an ICommand for execution.
    /// </summary>
    internal class CommandFactory
    {
        internal ICommand GetCommand(string[] args)
        {
            if (args.Length == 0)
            {
                return new CombineCSVCommand();
            }
            else if (args.Length == 1 && args[0] == "help")
            {
                return new HelpCommand();
            }
            else if (args.Length <= 5)
            {
                string inputPath = "";
                string outPutPath = "";
                int keyIndex = 1;
                char delimiter = ',';
                bool header = true;

                string[][] options = args.Select(x => x.Split('=')).ToArray();

                if (options.Select(x => x[0]).Distinct().Count() != options.Count())
                {
                    throw new Exception("Error: one or more options were used more than once.");
                }

                for (int i = 0; i < options.Length; i++)
                {
                    switch (options[i][0])
                    {
                        case "--input":
                            inputPath = options[i][1];
                            break;
                        case "--output":
                            outPutPath = options[i][1];
                            break;
                        case "--key":
                            if (!int.TryParse(options[i][1], out keyIndex))
                            {
                                throw new Exception($"Error: Please supply an integer for the primary key index");
                            }
                            break;
                        case "--delimiter":
                            if (!char.TryParse(options[i][1], out delimiter))
                            {
                                throw new Exception($"Error: Please supply a single character as the column delimiter");
                            }
                            break;
                        case "--header":
                            if (options[i][1].ToLower() == "y") header = true;
                            else if (options[i][1].ToLower() == "n") header = false;
                            else throw new Exception($"Please provide either Y or N for the header option");
                            break;
                        default:
                            throw new Exception($"Unrecognized option: \"{options[i][0]}\". Use the help command for more information.");
                    }
                }

                return new CombineCSVCommand(inputPath, outPutPath, keyIndex, delimiter, header);
            }

            else return new HelpCommand();
        }
    }
}
