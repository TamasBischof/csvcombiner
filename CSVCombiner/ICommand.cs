﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSVCombiner
{
    interface ICommand
    {
        void Execute();
    }
}
