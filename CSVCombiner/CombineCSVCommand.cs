﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace CSVCombiner
{
    class CombineCSVCommand : ICommand
    {
        string inputPath;
        string[] filesToCombine;
        int keyIndex;
        char splitChar;
        string outputPath;
        bool containsHeader;
        string currentHeader;
        Dictionary<int, string> lineBuffer = new Dictionary<int, string>();

        public CombineCSVCommand(string inputPath = "", string outputPath = "", int keyIndex = 1, char splitCharacter = ',', bool header = true)
        {
            this.inputPath = string.IsNullOrEmpty(inputPath) ? AppDomain.CurrentDomain.BaseDirectory : inputPath;
            this.outputPath = string.IsNullOrEmpty(outputPath) ? Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Output\\output.csv") : Path.Combine(outputPath, "output.csv");
            filesToCombine = Directory.GetFiles(this.inputPath, "*.csv");
            this.keyIndex = --keyIndex; //make it 0-based
            this.containsHeader = header;
            splitChar = splitCharacter;
        }

        public CombineCSVCommand() : this("", "", 1, ',')
        {

        }

        public void Execute()
        {
            if (!Directory.Exists(Path.GetDirectoryName(inputPath)))
            {
                throw new Exception($"Error: Input directory \"{inputPath}\" not found!");
            }
            Directory.CreateDirectory(Path.GetDirectoryName(outputPath));
            foreach (var file in filesToCombine)
            {
                var lines = File.ReadLines(file, Encoding.GetEncoding("iso-8859-1")).ToList();
                if (containsHeader)
                {
                    if (string.IsNullOrEmpty(currentHeader))
                    {
                        currentHeader = lines[0];
                    }
                    else if (currentHeader != lines[0])
                    {
                        throw new Exception($"Error: the header in {file} does not match the previously seen header \"{currentHeader}\"");
                    }

                    lines.RemoveAt(0);
                }
                foreach (var line in lines)
                {
                    string[] splitLine = line.Split(splitChar);
                    int id = 0;
                    if (splitLine.Length <= keyIndex || keyIndex < 0)
                    {
                        throw new Exception($"Error: the supplied key index ({keyIndex + 1}) is out of bounds");
                    }
                    if (!int.TryParse(splitLine[keyIndex], out id))
                    {
                        throw new Exception($"Error: Primary key could not be parsed:\nFile: {file}\nline: \"{line}\"\nkey index: {keyIndex+1}");
                    }
                    try
                    {
                        lineBuffer.Add(id, line);
                    }
                    catch (ArgumentException e)
                    {
                        throw new Exception($"Error: duplicate key detected: {file}, line: \"{line}\". This key already exists as \"{lineBuffer[id]}\"");
                    }
                }
            }

            try
            {
                File.WriteAllLines(outputPath, lineBuffer.OrderBy(x => x.Key).Select(x => x.Value).ToArray(), Encoding.GetEncoding("iso-8859-1"));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return;
            }

            Console.WriteLine($"Successfully combined {filesToCombine.Length} files to {outputPath}.");
        }
    }
}
